#!/bin/bash

regenerate_container () {
	# if docker container doesn't exis or we want to regenerate it.
	echo Generating docker container and starting
	npm run docker-clean
	npm run compile-docker-image
	npm run docker-start
}

regenerate_autos () {
	# if docker container doesn't exis or we want to regenerate it.
	echo Generating docker container and starting
	npm run docker-clean
	npm run compile-docker-image
	setup_autos
}

start() {
	# If docker container exists
	echo Compiling code and starting container
	npm run compile-docker-image
	npm run deploy
	npm run default
}

create_new_ui() {
	# If docker container exists
	echo Compiling code and starting container
	npm run compile-docker-image
	npm run default2
}

initial_setup() {
	echo Setting up initial configuration
	cd layers/functional-tests/
	npm i
	echo ${startgreen}`pwd`: npm i Done${endgreen}
	cd ../../
	npm i
	echo ${startgreen}`pwd`: npm i Done${endgreen}
	# npm run start
	npm run compile > /dev/null & # ?????
	pid=$! # Process Id of the previous running command

	spin='-\|/'

	i=0
	while kill -0 $pid
	do
	  i=$(( (i+1) %4 ))
	  printf "\r${spin:$i:1}"
	  sleep .1
	done
	echo ${startgreen}`pwd`: npm run compile Done${endgreen}
	mvn clean install -DskipTests=true -Pfunctional-tests  > /dev/null &
	pid=$! # Process Id of the previous running command

	spin='-\|/'

	i=0
	while kill -0 $pid
	do
	  i=$(( (i+1) %4 ))
	  printf "\r${spin:$i:1}"
	  sleep .1
	done

	echo 🎉 ${startgreen}mvn clean Done${endgreen}
}

check_repo(){
	if [[ ! -d "layers/functional-tests/" ]]
	then
	    echo "${startred}This path does not seem a repository to me.${endred}"
	    exit 1
	fi
}

setup_autos() {
	mvn clean install -DskipTests=true -Pfunctional-tests
	cd layers/functional-tests/
	npm install
	npm run jboss-mock
	#mvn clean install # Very important for tests to work
}

test_autos() {
  kill -9 $(lsof -t -i:10800);
  grunt chrome --profile=frontDev --test=intermediate_page/mobile/ipm_accommodation_details_manager.feature  --keepBrowser=1
}

# check time created container (to see if we need to regenerate as autos needs X time max.)


# Run autos:
# cd $REPO

# To test exit code of a command
# echo $?


# docker container start $(docker ps -a |grep portainer|tr -s [:space:] '\t'|cut -f 12)

usage()
{
    echo "usage: [[[-s Initial setup] [-r Recreate docker container] [-ra Recreate docker autos] [-b Start existing container] [-a Autos setup] [-c Start new ui]] | [-h This help]]"
}

startgreen=`tput setaf 2`
endgreen=`tput sgr0`
startred=`tput setaf 1`
endred=`tput sgr0`

if [ "$1" == "" ]
then
	usage
	exit 1
fi
check_repo
# while [ "$1" != "" ];
for arg in "$@"; do
    # case $1 in
    case $arg in
        -r | --regenerate )       #shift
                                #filename=$1
                                regenerate_container
                                ;;
        -ra | --regenerate_autos )       #shift
                                #filename=$1
                                regenerate_autos
                                ;;
        -s | --setup )          #shift
        			initial_setup
                                ;;
        -b | --boot )    	start
                                ;;
        -c | --create )    	create_new_ui
                                ;;
        -t | --test )    	test_autos
                                ;;
        -a | --autos )    	setup_autos
        ;;
        #-h | --help )          usage
        #                       exit
        #                       ;;
        #* )                     usage
        #                        exit 1
    esac
    shift
done
